#ifndef __STREAM_H
#define __STREAM_H

#include <stdlib.h>

typedef void * stream;

stream create_stream(int fd);
void free_stream(stream s);

char * stream_read_token_static(stream s);
char * stream_read_line_static(stream s, const char * delim);

void stream_feed_data(stream s, const char * data, size_t len);
void stream_feed_string(stream s, const char * data);
void stream_feed_fd(stream s, int fd);
void stream_flush(stream s);


#endif