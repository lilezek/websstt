#ifndef __HTTP_SERVER_H
#define __HTTP_SERVER_H

typedef int connection;
typedef unsigned short port;
typedef void * server;

server create_server(port p);
void free_server(server s);

connection wait_for_peer(server s);

#endif // __HTTP_SERVER_H