#include "stream.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

typedef struct {
	int fd;
	char * static_value;
	char * buffer;
	int rd_ptr;
	int fe_ptr;
	size_t buffer_s;
} stream_t;

static char * empty_string = "";

stream create_stream(int fd) {
	stream_t * s = malloc(sizeof(stream_t));
	s->fd = fd;
	s->static_value = NULL;
	s->buffer_s = 256;
	s->buffer = malloc(sizeof(char)*(s->buffer_s+1));
	s->rd_ptr = 0;
	s->fe_ptr = 0;
	return (stream)s;
}

void free_stream(stream sp) {
	stream_t * s = (stream_t*)sp;
	free(s->static_value);
	free(s->buffer);
	free(s);
}

void increase_buffer_size(stream sp) {
	stream_t * s = (stream_t*)sp;
	if (s->buffer_s <= 64) 
		s->buffer_s = 256;
	else 
		s->buffer_s = s->buffer_s*1.2;
	s->buffer = realloc(s->buffer,sizeof(char)*(s->buffer_s+1));
}

void srink_buffer(stream sp) {
	stream_t * s = (stream_t*)sp;
	int avail_data = s->fe_ptr - s->rd_ptr;
	if (s->fe_ptr == 0 && s->buffer_s < 256) {
		s->buffer_s = 256;
		s->buffer = realloc(s->buffer, sizeof(char)*(s->buffer_s+1));
		s->fe_ptr = s->rd_ptr = 0;
	}
	else if (s->rd_ptr > 0) {
		s->buffer_s = avail_data;
		char * nptr = (char*)malloc((s->buffer_s+1)*sizeof(char));
		memcpy(nptr, &s->buffer[s->rd_ptr], s->buffer_s);
		free(s->buffer);
		s->fe_ptr -= s->rd_ptr;
		s->rd_ptr = 0;
		s->buffer = nptr;
	}
}

int fetch_more_data(stream sp) {
	stream_t * s = (stream_t*)sp;
	int avail_space = s->buffer_s - s->fe_ptr;
	if (avail_space < 1) {
		increase_buffer_size(sp);
		return fetch_more_data(sp);
	}
	int rd = read(s->fd, &s->buffer[s->fe_ptr], avail_space);
	if (rd > 0) {
		s->fe_ptr += rd/sizeof(char);
		s->buffer[s->fe_ptr] = '\0';
	}
	return rd;
}

char * static_string_copy(stream sp, int start, int end) {
	stream_t * s = (stream_t*)sp;
	free(s->static_value);
	if (start >= end) {
		s->static_value = NULL;
		return empty_string;
	}
	int len = (end-start);
	s->static_value = malloc(sizeof(char)*(len+1));
	memcpy(s->static_value, &s->buffer[start], len);
	s->static_value[len] = '\0';
	return s->static_value;
}

char * find_first_space(stream sp) {
	stream_t * s = (stream_t*)sp;
	for (int i = s->rd_ptr; i < s->fe_ptr; i++) {
		if (isspace(s->buffer[i]))
			return &s->buffer[i];
	}
	return NULL;
}

char * stream_read_token_static(stream sp) {
	stream_t * s = (stream_t*)sp;
	int start = s->rd_ptr;
	int end = s->rd_ptr;
	char * found = NULL;
	int rd = 0;
	do {
		rd = fetch_more_data(sp);
		found = find_first_space(sp);
	} while (!found && rd > 0);
	if (found) {
		end = (found - s->buffer)/sizeof(char);
		char * result = static_string_copy(sp, start, end);
		if (result)
			s->rd_ptr = end+1;
		srink_buffer(sp);
		return result;
	}
	return NULL;
}

char * stream_read_line_static(stream sp, const char * delim) {
	stream_t * s = (stream_t*)sp;
	int start = s->rd_ptr;
	int end = s->rd_ptr;
	char * found = strstr(s->buffer, delim);
	int rd = 0;
	while (!found) {
		rd = fetch_more_data(sp);
		if (rd <= 0)
			break;
		found = strstr(s->buffer, delim);
	}
	if (found) {
		end = (found - s->buffer)/sizeof(char);
		char * result = static_string_copy(sp, start, end);
		if (result)
			s->rd_ptr = end+strlen(delim);
		srink_buffer(sp);
		return result;
	}
	return NULL;
} 

void stream_feed_data(stream sp, const char * data, size_t l) {
	stream_t * s = (stream_t*)sp;
	int avail_space = s->buffer_s - s->fe_ptr;
	while (l > avail_space) {
		increase_buffer_size(s);
		avail_space = s->buffer_s - s->fe_ptr;
	}
	memcpy(&s->buffer[s->fe_ptr], data, l);
	s->fe_ptr += l/sizeof(char);
	s->buffer[s->fe_ptr] = '\0';
}

void stream_feed_string(stream sp, const char * data) {
	size_t l = strlen(data);
	stream_feed_data(sp, data, l);
}

void stream_feed_fd(stream sp, int fd) {
	char buff[2096];
	int rd;
	while ((rd = read(fd, buff, 2095)) > 0) {
		buff[rd] = '\0';
		stream_feed_data(sp, buff, rd);
	}
	if (rd < 0)
		printf("Error: %s\n",strerror(errno));
}

void stream_flush(stream sp) {
	stream_t * s = (stream_t*)sp;
	const char * data = &s->buffer[s->rd_ptr];
	int len = s->fe_ptr - s->rd_ptr;
	int writen = 0;
	int act;
	do {
		act = write(s->fd, data, len-writen);
		writen += act;
	} while (len > writen && act > 0);
	s->fe_ptr = 0;
	s->rd_ptr = 0;
	srink_buffer(sp);
}