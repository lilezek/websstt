#include "request.h"
#include "stream.h"
#include <stdlib.h>

typedef struct {
	int method;
	char * document_path;
} request_t;

request create_request() {
	request_t * r = malloc(sizeof(request_t));
	r->method = HTTP_REQUEST_NOMETHOD;
	r->document_path = NULL;
	return r;
}

request read_http_request(connection c) {
	request_t * r = (request_t*)create_request();
	stream rdstr = create_stream(c);
	// Obtener el primer token, que debe ser el método:
	const char * met = stream_read_token_static(rdstr);

	// Establecer el método:
	if (!strcmp(met,"GET")) {
		r->method = HTTP_REQUEST_GET;
	}
	else if (!strcmp(met,"HEAD")) {
		r->method = HTTP_REQUEST_HEAD;
	}
	else if (!strcmp(met,"POST")) {
		r->method = HTTP_REQUEST_POST;
	}

	// Obtener el siguiente token, si todo ha ido bien, que debe ser
	// el camino:
	if (r->method == HTTP_REQUEST_NOMETHOD) {
		printf(0,"Error: método desconocido o inválido.");
		return NULL;
	}
	const char * path = stream_read_token_static(rdstr);
	// Copiar el camino a la estructura:
	set_document_path(r, path);
	// Ignorar el resto de los datos:
	const char * line;
	do {
		line = stream_read_line_static(rdstr,"\r\n");
	} while (strlen(line) > 0);
	return r;
}

void set_document_path(request re, const char * path) {
	request_t * r = (request_t*)re;
	free(r->document_path);
	r->document_path = malloc(sizeof(char)*(strlen(path)+2));
	r->document_path[0] = '.';
	strcpy(&r->document_path[1],path);
}

int get_request_method(request re) {
	request_t * r = (request_t*)re;
	return r->method;
}

const char * get_request_document(request re) {
	request_t * r = (request_t*)re;
	return r->document_path;	
}

void free_request(request re) {
	request_t * r = (request_t*)re;
	free(r->document_path);
	free(r);
}