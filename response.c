#include "response.h"
#include "list.h"
#include "stream.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <unistd.h>

typedef struct {
	char * mime_type;
	int fd;
} document_t;

typedef struct {
	int code;
	document_t * document;
} response_t;

document_t * create_document() {
	document_t * d = (document_t*)malloc(sizeof(document_t));
	d->mime_type = NULL;
	d->fd = -1;
	return d;
}

void free_document(document_t * d) {
	if (d) {
		free(d->mime_type);
		free(d);
	}
}

response create_response() {
	response_t * r = (response_t*)malloc(sizeof(response_t));
	r->code = HTTP_CODE_OK;
	r->document = NULL;
	return r;
}

size_t file_length(int fd) {
	size_t result = -1;
	struct stat st;
	if (!fstat(fd,&st))
		result = st.st_size;
	return result;	
}

void full_sendfile(int out_fd, int in_fd) {
	char buff[4096];
	int r;
	while (true) {
		r = read(in_fd, buff, 4096);
		if (r <= 0)
			break;
		write(out_fd, buff, r);
	}
}

// Connection only functions:
void write_http_response(response re, connection peer) {
	response_t * r = (response_t*)re;
	stream s = create_stream(peer);
	if (r->code == HTTP_CODE_OK) {
		stream_feed_string(s, "HTTP/1.1 200 OK\r\n");
	} else if (r->code == HTTP_CODE_NOTFOUND) {
		stream_feed_string(s, "HTTP/1.1 404 Not Found\r\n");
	} else if (r->code == HTTP_CODE_IMATEAPOT) {
		stream_feed_string(s, "HTTP/1.1 418 I'm a teapot\r\n");
	}
	stream_feed_string(s, "Connection: close\r\n");
	if (r->document != NULL) {
		char buffer[2048];
		sprintf(buffer, "Content-Type: %s\r\n", r->document->mime_type);
		stream_feed_string(s, buffer);
		size_t siz = file_length(r->document->fd);
		if (siz != -1) {
			sprintf(buffer, "Content-Length: %zd\r\n", siz);
			stream_feed_string(s, buffer);
		}
		stream_feed_string(s, "\r\n");
		stream_feed_fd(s, r->document->fd);
	}
	stream_flush(s);
	free_stream(s);
}

void set_response_code(response re, int code) {
	response_t * r = (response_t*)re;
	r->code = code;
}

void set_response_content(response re, int fd, const char * mime_type) {
	response_t * r = (response_t*)re;
	document_t * d = r->document;
	free_document(d);
	r->document = d = create_document();
	d->mime_type = malloc(sizeof(char)*(strlen(mime_type)+1));
	strcpy(d->mime_type,mime_type);
	d->fd = fd;
}

void free_response(response re) {
	response_t * r = (response_t*)re;
	if (r) {
		free_document(r->document);
		free(r);
	}
}