#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <syslog.h>
#include <sys/wait.h>
#include "server.h"
#include "request.h"
#include "response.h"

const char * default_404_file_path = "./404.html";

struct {
	char *ext;
	char *filetype;
} extensiones [] = {
	{"gif", "image/gif" },  
	{"jpg", "image/jpg" }, 
	{"jpeg","image/jpeg"},
	{"png", "image/png" },  
	{"ico", "image/ico" },  
	{"zip", "image/zip" },  
	{"gz",  "image/gz"  },  
	{"tar", "image/tar" },  
	{"htm", "text/html" },  
	{"html","text/html" },  
	{0,0} };

bool ends_with(const char * text, const char * pattern) {
	int text_final = strlen(text);
	int pattern_final = strlen(pattern);
	if (text_final < pattern_final) {
		return false;
	}
	for (const char * t1 = &text[text_final-pattern_final], * t2 = &pattern[0]; *t1 != '\0' && *t2 != '\0'; t1++,t2++) {
		if (*t1 != *t2)
			return false;
	}
	return true;
}

const char * get_mime_type(const char * file_path) {
	int it = 0;
	do {
		if (ends_with(file_path, extensiones[it].ext))
			return extensiones[it].filetype;
		it++;
	} while(extensiones[it].ext != 0);
	return NULL;
}


void daemonize( void (*functor)(int,char**), int argc, char ** argv, const char * path) {
	/*
	Call fork( ).
	*/
	int pid = fork();

	/*
		In the parent, call exit( ). This ensures that the original parent (the daemon's grandparent) is satisfied that its child terminated, that the daemon's parent is no longer running, and that the daemon is not a process group leader. This last point is a requirement for the successful completion of the next step.
	*/
	if(pid) {
		exit(0);
	}

	/*
	Call setsid( ), giving the daemon a new process group and session, both of which have it as leader. This also ensures that the process has no associated controlling terminal (as the process just created a new session, and will not assign one).
	*/

	if (setsid() < 0) {
		syslog(0,"No se pudo crear una nueva sesión");
		exit(127);
	}

	/*
	Change the working directory to the root directory via chdir( ). This is done because the inherited working directory can be anywhere on the filesystem. Daemons tend to run for the duration of the system's uptime, and you don't want to keep some random directory open, and thus prevent an administrator from unmounting the filesystem containing that directory.
	*/

	if (chdir(path) == -1) {
		syslog(0,"No se puede cambiar al directorio %s\n", path);
		exit(127);
	}

	/*
	Close all file descriptors.

	*/

	close(0); 
	close(1); 
	close(2);

	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);

	functor(argc, argv);
}

volatile bool exit_signal;

void sigint_handle(int sig) {
	if (sig == SIGINT)
		exit_signal = true;
}

/**
	void sigchld_handle(int sig) {
		int status;
		while(waitpid(-1, &status, WNOHANG) > 0); 
	}
*/

void reply_to_http_request(connection c) {
	request r;
	// Leer el request:
	if (r = read_http_request(c)) {
		int n = get_request_method(r);
		// Si es un request get
		if (n == HTTP_REQUEST_GET) {
			// Coger el documento que se quiere obtener
			const char * path = get_request_document(r);
			if (!strcmp(path,"./")) 
				path = "./index.html";
			int fd = open(path,O_RDONLY);
			int fd404 = open(default_404_file_path, O_RDONLY);
			response re = create_response();
			if (fd != -1) {
				// Generar un response 200 OK:
				set_response_code(re, 200);
				set_response_content(re, fd, get_mime_type(path));
			} else {
				// Generar un response 404 File not found:
				set_response_code(re, 404);
				set_response_content(re, fd404, get_mime_type(default_404_file_path));
			}
			write_http_response(re, c);
			if (fd != -1)
				close(fd);
			if (fd404 != -1) 
				close(fd404);
			free_response(re);
		}
		free_request(r);
	}
}

void handle_http_server_petitions(int argc, char ** argv) {
	int port = atoi(argv[1]);
	server s = create_server(port);

	signal(SIGINT,sigint_handle);
	signal(SIGCHLD, SIG_IGN);

	int pid;
	connection fd = -1;
	exit_signal = false;
	do {
		// Cerrar el anterior socket si es válido
		// Ya que estamos en el proceso padre que no va a tratar este socket
		if (fd != -1)
			close(fd);
		// Obtener en un proceso nuevo el descriptor del cliente:
		fd = wait_for_peer(s);
		pid = fork();
	} while (pid && !exit_signal && fd != -1);

	// En el proceso hijo:
	if (!pid) {
		if (fd != -1) {
			// Tratamos la conexión:
			reply_to_http_request(fd);
			// Cerramos el socket
			close(fd);
		}
		// Terminamos al hijo:
		exit(0);
	}

	closelog();
	free_server(s);
}

int main(int argc, char ** argv)
{
	
	if( argc < 3  || argc > 3 ) {
		printf("Uso: web-sstt numero-puerto directorio-web\n");
		printf("No se soporta: URLs que incluyan \"..\", Java, Javascript, CGI\n");
		exit(0);
	}

	daemonize(handle_http_server_petitions, argc, argv, argv[2]);
}