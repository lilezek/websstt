#ifndef __RESPONSE_H
#define __RESPONSE_H

#include "server.h"

#define HTTP_CODE_OK 		200
#define HTTP_CODE_NOTFOUND 	404
#define HTTP_CODE_IMATEAPOT 418

typedef void * response;

response create_response();

void set_response_code(response r, int code);
void set_response_content(response r, int file_content_descriptor, const char * mime_type);

// Connection only functions:
void write_http_response(response r, connection peer);
void free_response(response r);

#endif