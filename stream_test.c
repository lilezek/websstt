#include "stream.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

struct {
	char *ext;
	char *filetype;
} extensiones [] = {
	{"gif", "image/gif" },  
	{"jpg", "image/jpg" }, 
	{"jpeg","image/jpeg"},
	{"png", "image/png" },  
	{"ico", "image/ico" },  
	{"zip", "image/zip" },  
	{"gz",  "image/gz"  },  
	{"tar", "image/tar" },  
	{"htm", "text/html" },  
	{"html","text/html" },  
	{0,0} };

bool ends_with(const char * text, const char * pattern) {
	int text_final = strlen(text);
	int pattern_final = strlen(pattern);
	if (text_final < pattern_final) {
		return false;
	}
	for (const char * t1 = &text[text_final-pattern_final], * t2 = &pattern[0]; *t1 != '\0' && *t2 != '\0'; t1++,t2++) {
		if (*t1 != *t2)
			return false;
	}
	return true;
}

const char * get_mime_type(const char * file_path) {
	int it = 0;
	do {
		if (ends_with(file_path, extensiones[it].ext))
			return extensiones[it].filetype;
		it++;
	} while(extensiones[it].ext != 0);
	return NULL;
}


int main(void) {
	const char * s = get_mime_type("index.html");
	if (s)
		printf("%s\n",s);
	else
		printf("error\n");
	return 0;
}