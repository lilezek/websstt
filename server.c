#include "server.h"
#include "list.h"
#include "stream.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef struct {
	port p;
	list sockets;
	int passive_socket;
} server_t;


server create_server(port p) {
	// Alojar memoria
	server_t * s = malloc(sizeof(server_t));

	// Iniciar miembros:
	s->p = p;
	s->sockets = create_list();
	internal_tcp_setup(s);
	return 	(server)s;
}

void internal_tcp_setup(server sp) {
	server_t * s = (server_t*)sp;
	// Crear un nuevo socket y listarlo:
	int sock = s->passive_socket = socket(AF_INET, SOCK_STREAM, 0);

	// Si no se pudo crear el socket, morir:
	if (sock == -1) {
		syslog(0, "No se pudo crear el socket\n");
		syslog(0, "%s\n", strerror(errno));
		return;
	}

	// Generar el sockaddr:
	struct sockaddr_in sin;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(s->p);

	// Intentar el bind:
	if(bind(sock, (struct sockaddr *)&sin,sizeof(struct sockaddr_in) ) == -1) {
		syslog(0, "No se pudo hacer bind\n");
		syslog(0, "%s\n", strerror(errno));
	    return;
	}

	listen(sock, 12);
}

connection wait_for_peer(server sp) {
	// Recuperar estructura
	server_t * s = (server_t*)sp;

	// Y aceptar una conexión:
	int newsock;
	do {
		newsock = accept(s->passive_socket, NULL, NULL);
	} while (newsock == -1 && errno == EINTR);
	
	if (newsock == -1) {
		syslog(0, "No se pudo hacer accept\n");
		syslog(0, "%s\n", strerror(errno));
		return -1;
	}
	list_append_back(s->sockets, (void*)newsock);
	return newsock;
}

void free_server(server sp) {
	printf("Freeing server\n");
	server_t * s = (server_t*)sp;
	close(s->passive_socket);
	// Los sockets de la lista no hace falta cerrarlos
	// pues se cierran fuera.
	free_list(s->sockets);
	free(s);
}