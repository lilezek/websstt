#ifndef __REQUEST_H
#define __REQUEST_H 

#include "server.h"

#define HTTP_REQUEST_NOMETHOD -1
#define HTTP_REQUEST_HEAD 0
#define HTTP_REQUEST_GET 1
#define HTTP_REQUEST_POST 2

typedef void * request;

request create_request();
request read_http_request(connection c);

void free_request(request r);

int get_request_method(request r);
const char * get_request_document(request r);


#endif // __REQUEST_H 