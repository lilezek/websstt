#include "list.h"
#include <stdlib.h>
#include <stddef.h>

typedef struct {
	iterator_list head;
	iterator_list tail;
	size_t length;
} list_t;

typedef struct {
	iterator_list next;
	iterator_list past;
	void * element;
} iterator_list_t;

list create_list() {
	list_t * l = malloc(sizeof(list_t));
	l->head = NULL;
	l->tail = NULL;
	l->length = 0;
	return (list)l;
}

iterator_list list_append_back(list lp, void * element) {
	list_t * l = (list_t*)lp;
	iterator_list_t * it = malloc(sizeof(iterator_list_t));
	it->next = NULL;
	it->past = NULL;
	it->element = element;

	if (!l->length) {
		l->head = (iterator_list)it;
		l->tail = (iterator_list)it;
	} else {
		it->next = l->tail;
		l->tail = it;
	}

	l->length++;
	return (iterator_list)it;
}

void free_list(list lp) {
	// TODO: liberar los iteradores
	list_t * l = (list_t*)lp;
	free(l);
}