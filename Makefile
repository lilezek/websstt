
all: web_sstt

web_sstt: server.o stream.o list.o main.o request.o response.o
	gcc server.o stream.o list.o main.o request.o response.o -o web_sstt 

%.o: %.c
	gcc -std=c11 -c -w $< -O2
