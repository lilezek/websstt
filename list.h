#ifndef __LIST_H
#define __LIST_H

typedef void * list;
typedef void * iterator_list;

list create_list();
iterator_list list_append_back(list l, void * element);
iterator_list list_begin(list l);
void free_list(list l);

iterator_list list_iterator_next(iterator_list it);
void * list_iterator_get(iterator_list it);

#endif // __LIST_H